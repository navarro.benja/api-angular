import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from './services/loggerService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Aratech';
  list: any = [];

  constructor(
    private logger: LoggerService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    if (!this.logger.check()) {
      this.router.navigate(['/login']);
      return;
    }
  }
}
