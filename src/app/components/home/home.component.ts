import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from '../../services/loggerService'; // Nuestro servicio local

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  list: any = []; // Lista de usuarios
  pages: any = []; // Array con las paginas [{page:1}, {page:2},...]
  current_page: number = 0; // Pagina actual;

  constructor(
    private logger: LoggerService, // Inyectamos nuestro servicio
    private router: Router // Inyectamos el servicio router de angular
  ) { }

  /**
   * cuando se inicia el componente
   * 
   * @return  void
   */
  ngOnInit(): void {
    this.load();
  }

  /**
   * Carga los datos del back https://reqres.in/
   * 
   * @param page  number  numero de pagina a cargar, por defecto la primera
   */
  load(page: number = 1) {
    //console.log(page);
    this.logger.list(page).subscribe({
      next: l => {
        this.list = l.data;
        this.pages = Array(l.total_pages).fill(0).map((x, i) => ({ page: (i + 1) }));
        this.current_page = l.page;
      },
      error: (e: any) => console.error(e)
    });
  }

  /**
   * Cierra sesión (No he encontrado el end-point para cerrar sesion, asique lo que hacemos es eliminar el token)
   * @return  void
   */
  restoreSession(): void {
    this.logger.logout();
    this.router.navigate(['login']);
  }

}
