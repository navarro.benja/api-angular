import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoggerService } from 'src/app/services/loggerService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any;
  error: string | null = null;

  constructor( private logger: LoggerService, private router: Router ) {
    this.user = {
      email: 'eve.holt@reqres.in',
      password: 'cityslicka'
    }
  }

  ngOnInit(): void {
  }

  /**
   * Anclado al botón de entrar
   * Llama al método de nuestro servicio inyectado en toda la aplicación.
   * 
   * @return void
   */
  onSubmit(): void
  {
    this.logger.login(this.user).then(res => {
      console.log(res);
      this.router.navigate(['']);
    }).catch(e => {
      this.error = 'Hay errores en la petición:';
    })
  }

}
