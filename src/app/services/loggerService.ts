import { Injectable } from "@angular/core"; // Este es el service provider de angular.
import { HttpClient, HttpHeaders } from "@angular/common/http"; // Servicio Http
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private user: any; // Usuario actual
  private url: string = 'https://reqres.in/api/'; // La base para los end-points

  constructor(
    private httpClient: HttpClient
  )
  {
    this.user = JSON.parse(<string>localStorage.getItem('logger')) || {token: null};
  }

  /**
   * Chequea se hay usuario autentificado (con token)
   * 
   * @returns bolean
   */
  check(): boolean {
    return <boolean>!!this.user.token && this.user.token.length;
  }

  /**
   * Obtener el token
   * 
   * @param user Object 
   * @returns 
   */
  login(user: any) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.url + 'login', user).subscribe({
        next: (res: any) => {
          if (res.error)
            return reject();
          this.user = user;
          this.user.token = res.token;
          localStorage.setItem('logger', JSON.stringify(this.user));
          resolve(res.token)
        },
        error: reject
      });
    });
    
  }

  /**
   * 
   * @param page number Numero de pagina a cargar, por defecto la primera
   * @returns 
   */
  list(page: number = 1): Observable<any> {
    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Authorization': `Bearer ${this.user.token}`
    });

    return this.httpClient.get(`${this.url}users?page=${page}`, {headers: httpHeaders});
  }

  /**
   * Salida del usuario
   * 
   * @returns void
   */
  logout() {
    this.user.token = null;
    localStorage.setItem('logger', JSON.stringify(this.user));

  }
}