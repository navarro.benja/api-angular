// Importar los modulos de router de Angular
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Importar componentes
import { LoginComponent } from "./components/login/login.component";
import { HomeComponent } from "./components/home/home.component";

// Array de rutas
const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path:'login',
    component: LoginComponent
  }
];

export const appRoutingProvider: any[] = [];
export const routing: ModuleWithProviders<RouterModule> = RouterModule.forRoot(appRoutes);