Ejemplo de REST API call.

### Servicio utilizado
El servicio encargado de hacer las request es el [logger](https://gitlab.com/navarro.benja/api-angular/-/blob/main/src/app/services/loggerService.ts) en la carpeta /services.

Con el decorador `@Injectable` y parámetro `providerIn: 'root'` inyectamos una sola instancia para toda la aplicación.

Los métodos son:

* check() `true/false` Si la app tiene token o no.
* login(`{email, password}`) `Promise` 
* list(`page:number`) `Observable` Lista los usuarios
* logout() `void` Cierra sesión

### Ramas
* main - Aplicación sencillita
* models - main + agregación de modelos simples
* guard - main + models + interceptores http.

------

[Benjamin Navarro][home]
------

[home]: <https://gitlab.com/navarro.benja> "Ver mas"
